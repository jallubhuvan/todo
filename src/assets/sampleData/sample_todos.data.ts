export const todos = [
	{
		email: "bhuvan@todo.com",
		todos: [
			{
				id: "23f9aba9-a3b6-209c-1ac5-0e719959cf1c",
				completed: true,
				todo: "Complete KYC work. "
			},
			{
				id: "23f9aba9-a3b6-209c-1ac5-0e719959cf1q",
				completed: false,
				todo: "Renew bike licence "
			}, 
			{
				id: "25f9aba9-a3b6-209c-1ac5-0e719959cf1c",
				completed: false,
				todo: "Cancel flight ticket"
			}
		]
	}, 
	{
		email: "kiran@todo.com",
		todos: [
			{
				id: "26f9aba9-a3b6-209c-1ac5-0e719959cf1c",
				completed: true,
				todo: "Collect groceries"
			},
			{
				id: "27f9aba9-a3b6-209c-1ac5-0e719959cf1c",
				completed: true,
				todo: "Packing bags"
			}, 
			{
				id: "28f9aba9-a3b6-209c-1ac5-0e719959cf1c",
				completed: "",
				todo: "Call Manohar"
			}
		]
	},
	{
		email: "kumar@todo.com",
		todos: [
			{
				id: "29f9aba9-a3b6-209c-1ac5-0e719959cf1c",
				completed: false,
				todo: "Go through the course"
			},
			{
				id: "30f9aba9-a3b6-209c-1ac5-0e719959cf1c",
				completed: false,
				todo: "Appear for test"
			}, 
			{
				id: "31f9aba9-a3b6-209c-1ac5-0e719959cf1c",
				completed: false,
				todo: "Check results"
			}
		]
	}
]