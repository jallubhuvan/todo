import { Action } from '@ngrx/store'
import {Task} from './../models/task.model';

export const ADD_TODO = 'add';
export const SET_USER_DATA = 'userdata';
export const REMOVE_TODO = 'remove';
export const TOGGLE_TODO = 'toggle';
export const COMPLETE_ALL_TODOS = 'completeAll';
export const REMOVE_ALL_COMPLETED = 'removeAllCompleted'


export class AddTodo implements Action {
    readonly type = ADD_TODO;
    constructor(public payload: Task) {}
}

export class SetInitialTodo implements Action {
    readonly type = SET_USER_DATA;
    constructor(public payload: Task[]) {}
}

export class RemoveTodo implements Action {
    readonly type = REMOVE_TODO;
    constructor(public payload: string) {}
}
export class ToggleTodo implements Action {
    readonly type = TOGGLE_TODO;
    constructor(public payload: string) {}
}

export class CompleteAllTodos implements Action {
    readonly type = COMPLETE_ALL_TODOS;
}

export class RemoveAllCompleted implements Action {
    readonly type = REMOVE_ALL_COMPLETED;
}


export type Actions = AddTodo | SetInitialTodo | RemoveTodo | ToggleTodo | CompleteAllTodos | RemoveAllCompleted