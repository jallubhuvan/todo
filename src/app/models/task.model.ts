    export interface Task {
        todo: string,
        completed: boolean | string,
        id: string
    }