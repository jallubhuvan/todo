import { Task } from "../models/task.model";
import * as TodoActions from './../actions/todo.actions'


const initialState: Task = {id: "", completed: true, todo: 'Display all todos'}

export function reducer(state: Task[] = [initialState], action: TodoActions.Actions) {

    // Section 3
    switch(action.type) {
        case TodoActions.ADD_TODO:
            return [...state, action.payload];

        case TodoActions.SET_USER_DATA:
            return [...action.payload];

        case TodoActions.REMOVE_TODO:
            let removedTaskIndex = state.findIndex(task => task.id === action.payload)
            let existingTodos: Task[] = JSON.parse(JSON.stringify(state));
            if(removedTaskIndex !== -1) {
                existingTodos.splice(removedTaskIndex,1);
            }
            return [...existingTodos];

        case TodoActions.TOGGLE_TODO:
            let currentState: Task[] = JSON.parse(JSON.stringify(state));
            let toggleTaskIndex = currentState.findIndex(task => task.id === action.payload)
            if(toggleTaskIndex !== -1) {
                currentState[toggleTaskIndex].completed = !currentState[toggleTaskIndex].completed;
            }
            return [...currentState];

        case TodoActions.COMPLETE_ALL_TODOS:
            let allTodos: Task[] = JSON.parse(JSON.stringify(state));
            allTodos.map(task => task.completed = true)
            return [...allTodos];

        case TodoActions.REMOVE_ALL_COMPLETED:
            return [...state.filter(todo => !todo.completed)];

        default:
            return state;
    }
}