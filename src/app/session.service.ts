import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  public userEmail: string = '';
  title: string = 'Todo - App'

  setUserEmail(value: string) {
    this.userEmail = value;
  }

  getUserEmail() {
    return  this.userEmail;
  }
  
}
