import { TestBed } from '@angular/core/testing';

import { SessionService } from './session.service';

describe('SessionService', () => {
  let service: SessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SessionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be set user email to abc@todo.com', () => {
    service.setUserEmail('abc@todo.com')
    expect(service.userEmail).toEqual('abc@todo.com');
  });

  it('should get user email', () => {
    service.setUserEmail('abc@todo.com')
    expect(service.getUserEmail()).toEqual('abc@todo.com');
  });
});
