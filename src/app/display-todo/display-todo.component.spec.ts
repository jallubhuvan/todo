import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { SessionService } from '../session.service';
import { spy, stub } from 'sinon';

import { DisplayTodoComponent } from './display-todo.component';
import { FormsModule } from '@angular/forms';
import { FilterTodosPipe } from '../pipes/filter-todos.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { Task } from '../models/task.model';

describe('DisplayTodoComponent', () => {
  let component: DisplayTodoComponent;
  let fixture: ComponentFixture<DisplayTodoComponent>;
  let routeStub = {
    navigate: spy()
  }

  let sessionStub = {
    setUserEmail: spy(),
    getUserEmail: stub().returns('')
  }

  let storeStub = {
    select: stub().resolves([{id: "", completed: true, todo: 'Display todos'}]),
    dispatch: stub().returns([])
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule,
        TranslateModule.forRoot()],
      declarations: [ DisplayTodoComponent, FilterTodosPipe ],
      providers: [
          {provide:SessionService, useValue: sessionStub}, 
          {provide: Router, useValue: routeStub},
          {provide: Store, useValue: storeStub}
        ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should remove user from session once user clicked on logout', () => {
    component.doLogout();
    expect(component.sessionService.getUserEmail()).toEqual('');
  });

  it('should set filter to displayAll', () => {
    component.displayAllTasks();
    expect(component.filterType).toEqual('displayAll');
  });

  it('should set filter to displayCompleted', () => {
    component.displayCompletedTasks();
    expect(component.filterType).toEqual('displayCompleted');
  });

  it('should set filter to displayActive', () => {
    component.displayActiveTasks();
    expect(component.filterType).toEqual('displayActive');
  });

  it('should remove all completed', () => {
    component.clearCompleted();
    let dispatchResponse: Task[]; 
    storeStub.dispatch((res:any) => {
      dispatchResponse = res
      expect(dispatchResponse).toEqual([]);
    });
  });
});
