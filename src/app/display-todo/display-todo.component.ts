import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Task } from '../models/task.model';
import { SessionService } from '../session.service';
import { UUID } from 'angular2-uuid';
import {todos } from './../../assets/sampleData/sample_todos.data';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import * as TodoActions from './../actions/todo.actions'

@Component({
  selector: 'app-display-todo',
  templateUrl: './display-todo.component.html',
  styleUrls: ['./display-todo.component.scss']
})
export class DisplayTodoComponent implements OnInit { 


  userEmail: string = '';
  todos: Observable<Task[]>;
  filterType: string = 'displayAll';
  taskList:Task[] = [];
  newTask: Task = {todo: '', completed: false, id: ''};
  isCheckedAll = false;
  

  constructor(readonly sessionService: SessionService, readonly router: Router, private store: Store<AppState>) { 
    //todos is an observable holding the value of state. We are displaying the todos by using async pipe
    this.todos = this.store.select('todos');
  }

  ngOnInit(): void {
    console.log(UUID.UUID());
    let loggedInUser = this.sessionService.getUserEmail();
    if(!loggedInUser) {
      this.router.navigate(['/login']);
    } else {
      this.userEmail = loggedInUser;
      let loggedInUserTodos = todos.find(user => user.email === this.userEmail);
      if(loggedInUserTodos) {
        this.taskList = [...loggedInUserTodos.todos];
      }
      this.store.dispatch(new TodoActions.SetInitialTodo(this.taskList));
    }
    }
  
  /**
   * Navigates user to the login page
   */ 
  doLogout() {
    this.sessionService.setUserEmail('');
    this.router.navigate(['/login']); 
  }

  /**
   * 
   * @param id : string
   * It will toggle the state of todo status for the seleted todo
   */
  completedTask(id: string) {
    this.store.dispatch(new TodoActions.ToggleTodo(id));
    
  }

  /**
   * It will change the status of  all the todos to completed
   */
  checkAll() {
    this.isCheckedAll = !this.isCheckedAll;
    if(this.isCheckedAll) {
      this.store.dispatch(new TodoActions.CompleteAllTodos());
    }
  }

  /**
   * It will remove all the completed todos
   */
  clearCompleted() {
    this.store.dispatch(new TodoActions.RemoveAllCompleted());
  }

  /**
   * 
   * @param id Id of the selected todo
   * Based on the selected todo id it will remove the todo from the list
   */
  removeSelected(id: string) {
    this.store.dispatch(new TodoActions.RemoveTodo(id));
  }

  /**
   * 
   * @param event todo decscription
   * It will add new todo the existing todos. By default the status of the newly
   * added todo will be incomplete
   */
  addNewTask(event: any) {
    if(this.newTask.todo.trim() && event.key === 'Enter') {
      this.newTask.id = UUID.UUID();
      this.store.dispatch(new TodoActions.AddTodo(this.newTask));
      this.newTask = {todo: '', completed: false, id: ''};
    }
  }

  /**
   * It will set the param value which will be sent to fitlerTodo pipe to displayAll
   */
  displayAllTasks() {
    this.isCheckedAll = false;
    this.filterType = 'displayAll';
  }

  /**
   * It will set the param value which will be sent to fitlerTodo pipe to displayActive
   */
  displayActiveTasks() {
    this.isCheckedAll = false;
    this.filterType = 'displayActive';

  }

    /**
   * It will set the param value which will be sent to fitlerTodo pipe to displayCompleted.
   */
  displayCompletedTasks() {
    this.isCheckedAll = false;
    this.filterType = 'displayCompleted';

  }

}
