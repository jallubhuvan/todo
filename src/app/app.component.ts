import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'todo';
  color = '#337ab7'

  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');
    
    //setting default color to #337ab7
    document.documentElement.style.setProperty('--primary-color', this.color);
  }

  /**
   * 
   * @param event selected color hex code
   * Setting the selected hex code to the variable '--primary-color'in styless.scss
   */
  changeCol(event: any) {
    console.log(event);
    document.documentElement.style.setProperty('--primary-color', event);
  }
}
