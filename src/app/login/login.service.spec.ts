import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { stub } from 'sinon';

import { LoginService } from './login.service';

describe('LoginService', () => {
  let service: LoginService;
  let httpStub = {
    get: stub().returns(of({}))
  }

  beforeEach(() => {
    TestBed.configureTestingModule({providers: [{provide: HttpClient, useValue: httpStub}]});
    service = TestBed.inject(LoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get users mock data', () => {
    let mockUsers = service.getUserDetailsMock();
    expect(mockUsers.length).toEqual(3);
  });

  it('should fetch data from mock api', () => {
    let userDetails;
    userDetails = service.getUserDetails();
    userDetails.subscribe(res => {
      expect(res).toEqual({});
    })
  })

});
