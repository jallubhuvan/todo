import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { SessionService } from '../session.service';
import { spy, stub } from 'sinon';
import { LoginComponent } from './login.component';
import { LoginService } from './login.service';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const routeStub = {
    navigate: spy()
  };

  const loginServiceStub = {
      getUserDetails:stub().returns(of({}))
  }

  const sessionServiceStub = {
    setUserEmail: spy()
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ LoginComponent ],
      providers: [{provide: SessionService, useValue: sessionServiceStub}, {provide:LoginService, useValue: loginServiceStub}, {provide:Router, useValue: routeStub}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set error message to wrong credentials', () => {
    component.setErrorMessage('wrong credentials')
    expect(component.errorMessage).toEqual('wrong credentials');
  });
});
