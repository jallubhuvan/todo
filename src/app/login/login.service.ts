import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';

const URL = 'https://demo4874727.mockable.io/getuserdetails';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public http: HttpClient) { }

  //To get user details.
  getUserDetails() {
    return this.http.get(URL);
  }

  // Using mock data incase if response is not returned from the endpoint.
  getUserDetailsMock() {
    return [
      {
      "name": "Bhuvan",
      "email": "bhuvan@todo.com",
      "password": "password"
      },
      {
      "name": "Kiran",
      "email": "kiran@todo.com",
      "password": "password"
      },
      {
      "name": "Kumar",
      "email": "kumar@todo.com",
      "password": "password"
      }
    ];
  }
  

  // URL: https://www.mockable.io/a/#/space/demo4874727/rest/Wt7e86AAA 
}
