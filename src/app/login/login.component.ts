import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '../session.service';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string = '';
  password: string = '';
  errorMessage: string = '';

  constructor(readonly sessionService: SessionService, readonly router: Router,readonly loginService: LoginService) { }

  ngOnInit(): void {
    this.sessionService.setUserEmail('');
  }

  /**
   * 
   * @param formDetails user given values
   * It will call validate method once user clicked on login and also fetch the uses details.
   */
  doLogin(formDetails: NgForm) {
    this.setErrorMessage('');
    this.loginService.getUserDetails().subscribe((res: any) => {
      if(res) { 
        this.validateUser(res, formDetails);
      }
    },
    error => {
      console.error(error);
      if(error?.status === 404) {
          let userDetailsMock = this.loginService.getUserDetailsMock();
          this.validateUser(userDetailsMock, formDetails);
      } else {
        this.setErrorMessage('Something went wrong');
      }
      
    })
  }

  /**
   * 
   * @param message error message
   * Sets error message string
   */
  setErrorMessage(message: string) {
    this.errorMessage = message;
  }

  /**
   * 
   * @param res Will contains login details of the users in a list
   * @param formDetails login details provided by user
   * If matched user will be navigated to  todos page.
   * If not matched error will be displayed to the user.
   */
  validateUser(res:any, formDetails: NgForm) {
    let userGivenValues = formDetails.value
    let userLoggingIn = res.find((user: { email: any; password: any; }) => (user.email === userGivenValues.email) && (user.password === userGivenValues.password));
    if(userLoggingIn) {
      this.sessionService.setUserEmail(userGivenValues.email);
      this.router.navigate(['/todo']);
    } else {
        this.setErrorMessage('Wrong Credentials.');
    }

  }

}
