import { Pipe, PipeTransform } from '@angular/core';
import { Task } from '../models/task.model';

@Pipe({
  name: 'filterTodos'
})
export class FilterTodosPipe implements PipeTransform {

  transform(todos: Task[]| null, filterType: string = 'displayAll'): Task[] {
    console.log('FilterType: ', filterType);

    if(todos && todos.length > 0) {
      switch(filterType) {
        case 'displayAll':
          return todos;
          break;
        case 'displayActive':
          return todos.filter(todo => !todo.completed);
          break;

        case 'displayCompleted': 
          return todos.filter(todo => todo.completed);
          break;

        default:
          return []
      }
    }
    return [];
  }

}
