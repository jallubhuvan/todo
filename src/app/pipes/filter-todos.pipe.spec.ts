import { FilterTodosPipe } from './filter-todos.pipe';

describe('FilterTodosPipe', () => {
  it('create an instance', () => {
    const pipe = new FilterTodosPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return filered data', () => {
    const pipe = new FilterTodosPipe();
    let filteredData = pipe.transform([{id: '', completed: true, todo: 'filter'}, {id: '', completed: false, todo: 'displayCompleted'}], 'displayCompleted');
    expect(filteredData.length).toEqual(1);

  })
});
