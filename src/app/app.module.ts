import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule } from '@angular/forms';
import { DisplayTodoComponent } from './display-todo/display-todo.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {  TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ColorPickerModule } from 'ngx-color-picker';
import { StoreModule } from '@ngrx/store';
import { reducer } from './reducers/todo.reducer';
import { FilterTodosPipe } from './pipes/filter-todos.pipe';

export function HttpLoaderFactory(http:HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DisplayTodoComponent,
    FilterTodosPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ColorPickerModule,
    TooltipModule.forRoot(),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }}),
      StoreModule.forRoot({
        todos: reducer
      })
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
