import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ColorPickerModule } from 'ngx-color-picker';
import { spy, stub } from 'sinon';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let translateServiceStub = {
    addLangs: spy(),
    setDefaultLang:spy(),
    getBrowserLang: stub().returns('')

  }
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
        ColorPickerModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [{provide: TranslateService, useValue: translateServiceStub}]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'todo'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('todo');
  });

});
